﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RunningCatsTotalizator
{
    class Cats
    {
        public static int totalCatsInTheGame = 10;
        public int catNumber = 0, catSpeed = 0;
        public double CatTime(int distance, int catSpeed)
        {
            return distance / catSpeed;
        }
    }
}
