﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RunningCatsTotalizator
{
    class Program
    {
        const int distance = 1000;
        static double bestTime = 0;
        static int playerPrediction;
        static void Main(string[] args)
        {
            int winNumber;
            ShowStartGameMenu();
            StartGame(out winNumber);
            Console.ReadKey();
        }
        static void ShowStartGameMenu()
        {
            Console.WriteLine("Добро пожаловать в \"Симулятор кошачьих бегов версии\" ВЕРСИЯ 1.0!");
            Console.WriteLine("Правила очень просты: у нас есть {0} котов, участвующих в забеге!", Cats.totalCatsInTheGame);
            Console.WriteLine("Каждый из котов имеет свой уникальный номер. Вам нужно угадать кто прибежит к финишу первым.");
            Console.WriteLine("Для этого достаточно просто ввести его номер с клавиатуры и узнать угадали или нет!");
            Console.WriteLine("Хотите сыграть? (введите \"да\" чтобы сыграть или \"нет\", чтобы завершить игру)");
        }

        static bool GetPlayerPrediction(out int pPrediction)
        {
            pPrediction = 0;
            while (true)
            {
                string mes = Console.ReadLine();
                if (mes.Equals("да", StringComparison.OrdinalIgnoreCase))
                {
                    Console.Clear();
                    Console.WriteLine("Отлично!!! Забег вот вот начнется, какой номер по вашему мнению выиграет ? Укажите пожалуйста число от 1 до {0}", Cats.totalCatsInTheGame);
                    while (true)
                    {
                        if (Int32.TryParse(Console.ReadLine(), out pPrediction))
                        {
                            if (pPrediction >= 1 && pPrediction <= Cats.totalCatsInTheGame)
                            {
                                break;
                            }
                        }
                        Console.WriteLine("Вам следует ввести число из указанного диапазона!");
                    }
                    Console.WriteLine("Спортсмены готовы! Нажмите любую клавишу чтобы начать забег!");
                    Console.ReadKey();
                    return true;
                }
                else if (mes.Equals("нет", StringComparison.OrdinalIgnoreCase))
                {
                    Console.Clear();
                    Console.WriteLine("Очень жаль! До встречи!");
                    return false;
                }
                else
                {
                    Console.WriteLine("Вы ввели что - то не то!Давайте еще раз...");
                    continue;
                }
            }
        }

        static void StartGame(out int winNumber)
        {
            winNumber = 0;
            if (GetPlayerPrediction(out playerPrediction))
            {
                Random rnd = new Random();
                for (int i = 1; i <= Cats.totalCatsInTheGame; i++)
                {
                    int r = rnd.Next(1, 11);
                    Cats cats = new Cats();
                    cats.catNumber = i;
                    cats.catSpeed = 10 * r;
                    double result = cats.CatTime(distance, cats.catSpeed);
                    if (bestTime == 0)
                        bestTime = result;
                    if (result <= bestTime)
                        winNumber = cats.catNumber;
                }
                Console.WriteLine("Победил кот под номером: {0}, пробежав дистанцию за = {1} секунд!", winNumber, bestTime);

                if (playerPrediction == winNumber)
                {
                    Console.WriteLine("ПОЗДРАВЛЯЕМ!!! Вы угадали!");
                    Console.WriteLine("Спасибо за игру!До встречи");
                }
                else
                {
                    Console.WriteLine("Очень жаль, но вы проиграли!");
                    Console.WriteLine("В следующий раз непременно повезет!До встречи!");
                }
            }
        }
    }
}
